import asyncio
import logging
import sys
from os import getenv

from aiogram import Bot, Dispatcher
from aiogram.client.default import DefaultBotProperties
from aiogram.enums import ParseMode
from aiogram.types import Message


TOKEN = getenv("TOKEN")
MESSAGE_ADDED_TEXT = getenv("MESSAGE_ADDED_TEXT")


dp = Dispatcher()

@dp.channel_post()
async def channel_post_handler(message: Message):
    edited_text = message.md_text + "\n" + MESSAGE_ADDED_TEXT
    try:
        await message.edit_text(text=edited_text, parse_mode=ParseMode.MARKDOWN_V2, disable_web_page_preview=True)
    except:
        await message.edit_caption(caption=edited_text, parse_mode=ParseMode.MARKDOWN_V2, disable_web_page_preview=True)

async def main() -> None:
    bot = Bot(token=TOKEN, default=DefaultBotProperties(parse_mode=ParseMode.HTML))
    await dp.start_polling(bot)


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO, stream=sys.stdout)
    asyncio.run(main())
